<?php

namespace App\ApiResource\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPasswordHasher implements ProcessorInterface
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(
        private readonly ProcessorInterface  $itemPersisterProcessor,
        UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data->getPlainPassword()) {
            $hashedPassword = $this->passwordEncoder->hashPassword($data, $data->getPlainPassword());
            $data->setPassword($hashedPassword);
        }

        return $this->itemPersisterProcessor->process($data,  $operation,  $uriVariables, $context );
    }
}
